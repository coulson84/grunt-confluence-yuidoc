'use strict';
import babelPolyfill from 'babel-polyfill';
import url from 'url';
import path from 'path';
import util from 'util';
import fs from 'fs';
import {
    getMainPage,
    getPageFromTitle,
    createPage,
    uploadUnder,
    deletePage
} from './confluence';

const fileSymbol = Symbol('files');
const titlePrefixes = {
    modules: 'Module - ',
    classes: 'Class - ',
    elements: 'Element - ',
    files: 'File - '
};

function getTitle(type, version, filename) {
    return `${version ? `(${version}) ` : ''}${titlePrefixes[type]}${filename}`;
}

function updateWikiLinks(content, version) {
    return content.replace(/\[[\s\S]*?\|([\s\S]*?)\]/g, function(match, c1) {
        if(/^(#|https?:)/.test(c1)) {
            return match;
        } else {
            return match.replace(c1, getTitle(c1.split('/')[1], version, path.parse(c1).name));
        }
    });
}

export default function(grunt) {

    function createVersionPage(parentId, confluenceOptions) {
        return createPage(parentId, confluenceOptions.version, confluenceOptions.spaceKey, confluenceOptions.directoryContent || '', confluenceOptions);
    }

    function getStartPage(confluenceOptions) {
        return Promise.all([
                getMainPage(confluenceOptions),
                confluenceOptions.title ? getPageFromTitle(confluenceOptions) : 'not required',
                confluenceOptions.version ? getPageFromTitle(Object.assign({}, confluenceOptions, {
                    title: confluenceOptions.version
                })) : 'not required'
            ])
            .then(([mainPage, titlePage, versionPage]) => {
                if (!mainPage) {
                    grunt.error.fatal('Could not find space on Confluence');
                }

                if (confluenceOptions.title) {
                    return createPage(mainPage.id, confluenceOptions.title, confluenceOptions.spaceKey, confluenceOptions.directoryContent || '', confluenceOptions)
                        .then(titlePage => {
                            if (!confluenceOptions.version) {
                                return titlePage;
                            } else {
                                return createVersionPage(titlePage.id, confluenceOptions);
                            }
                        });
                } else if (confluenceOptions.version) {
                    return createVersionPage(mainPage.id, confluenceOptions);
                }
            });
    }

    grunt.registerMultiTask('confluenceUpload', 'Upload documentation to Confluence', function() {
        let complete = this.async();
        let confluenceOptions = Object.assign({}, {
            username: process.env.CONFLUENCE_USERNAME,
            password: process.env.CONFLUENCE_PASSWORD,
            baseUrl: process.env.CONFLUENCE_URL,
            spaceKey: process.env.CONFLUENCE_SPACE,
            title: process.env.CONFLUENCE_TITLE,
            version: process.env.CONFLUENCE_DOCUMENTATION_VERSION,
            fileSymbol
        }, this.options().confluence);

        if (url.parse(confluenceOptions.baseUrl).path === '/') {
            confluenceOptions.baseUrl += '/wiki';
        }

        if(!confluenceOptions.username) {
            grunt.error.fatal('username is a required option');
        }

        if(!confluenceOptions.password) {
            grunt.error.fatal('password is a required option');
        }

        if(!confluenceOptions.spaceKey) {
            grunt.error.fatal('spaceKey is a required option');
        }

        if(!confluenceOptions.baseUrl) {
            grunt.error.fatal('baseUrl is a required option')
        }

        let fileMap = this.files[0].src.reduce((map, file) => {
            let dirs = file.split('/').slice(1);
            let descent = map;
            let step = '';

            while(dirs.length > 1) {
                step = dirs.shift();
                if (step === 'assets') {
                    return map;
                }

                descent = descent[step] = descent[step] || { [fileSymbol]: [] };
            }

            if(dirs.length && !grunt.file.isDir(file) && step) {
                descent[fileSymbol].push({ path: file, type: step, content: updateWikiLinks(fs.readFileSync(file, 'utf-8'), confluenceOptions.version), title: getTitle(step, confluenceOptions.version, path.parse(file).name) });
            }
            return map;
        }, { [fileSymbol]: []});

        getStartPage(confluenceOptions)
        .then(page => uploadUnder(page, confluenceOptions, fileMap))
        .then(({ existing, written }) => Promise.all(Array.from(existing).filter(page => !written.has(page)).map(page => deletePage(page, confluenceOptions))))
        .then(() => {
            return complete();
        })
        .catch(err => {
            grunt.fail.fatal(err);
        });
    });
}
