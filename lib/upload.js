'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.default = function (grunt) {

    function createVersionPage(parentId, confluenceOptions) {
        return (0, _confluence.createPage)(parentId, confluenceOptions.version, confluenceOptions.spaceKey, confluenceOptions.directoryContent || '', confluenceOptions);
    }

    function getStartPage(confluenceOptions) {
        return Promise.all([(0, _confluence.getMainPage)(confluenceOptions), confluenceOptions.title ? (0, _confluence.getPageFromTitle)(confluenceOptions) : 'not required', confluenceOptions.version ? (0, _confluence.getPageFromTitle)(Object.assign({}, confluenceOptions, {
            title: confluenceOptions.version
        })) : 'not required']).then(function (_ref) {
            var _ref2 = _slicedToArray(_ref, 3);

            var mainPage = _ref2[0];
            var titlePage = _ref2[1];
            var versionPage = _ref2[2];

            if (!mainPage) {
                grunt.error.fatal('Could not find space on Confluence');
            }

            if (confluenceOptions.title) {
                return (0, _confluence.createPage)(mainPage.id, confluenceOptions.title, confluenceOptions.spaceKey, confluenceOptions.directoryContent || '', confluenceOptions).then(function (titlePage) {
                    if (!confluenceOptions.version) {
                        return titlePage;
                    } else {
                        return createVersionPage(titlePage.id, confluenceOptions);
                    }
                });
            } else if (confluenceOptions.version) {
                return createVersionPage(mainPage.id, confluenceOptions);
            }
        });
    }

    grunt.registerMultiTask('confluenceUpload', 'Upload documentation to Confluence', function () {
        var complete = this.async();
        var confluenceOptions = Object.assign({}, {
            username: process.env.CONFLUENCE_USERNAME,
            password: process.env.CONFLUENCE_PASSWORD,
            baseUrl: process.env.CONFLUENCE_URL,
            spaceKey: process.env.CONFLUENCE_SPACE,
            title: process.env.CONFLUENCE_TITLE,
            version: process.env.CONFLUENCE_DOCUMENTATION_VERSION,
            fileSymbol: fileSymbol
        }, this.options().confluence);

        if (_url2.default.parse(confluenceOptions.baseUrl).path === '/') {
            confluenceOptions.baseUrl += '/wiki';
        }

        if (!confluenceOptions.username) {
            grunt.error.fatal('username is a required option');
        }

        if (!confluenceOptions.password) {
            grunt.error.fatal('password is a required option');
        }

        if (!confluenceOptions.spaceKey) {
            grunt.error.fatal('spaceKey is a required option');
        }

        if (!confluenceOptions.baseUrl) {
            grunt.error.fatal('baseUrl is a required option');
        }

        var fileMap = this.files[0].src.reduce(function (map, file) {
            var dirs = file.split('/').slice(1);
            var descent = map;
            var step = '';

            while (dirs.length > 1) {
                step = dirs.shift();
                if (step === 'assets') {
                    return map;
                }

                descent = descent[step] = descent[step] || _defineProperty({}, fileSymbol, []);
            }

            if (dirs.length && !grunt.file.isDir(file) && step) {
                descent[fileSymbol].push({ path: file, type: step, content: updateWikiLinks(_fs2.default.readFileSync(file, 'utf-8'), confluenceOptions.version), title: getTitle(step, confluenceOptions.version, _path2.default.parse(file).name) });
            }
            return map;
        }, _defineProperty({}, fileSymbol, []));

        getStartPage(confluenceOptions).then(function (page) {
            return (0, _confluence.uploadUnder)(page, confluenceOptions, fileMap);
        }).then(function (_ref4) {
            var existing = _ref4.existing;
            var written = _ref4.written;
            return Promise.all(Array.from(existing).filter(function (page) {
                return !written.has(page);
            }).map(function (page) {
                return (0, _confluence.deletePage)(page, confluenceOptions);
            }));
        }).then(function () {
            return complete();
        }).catch(function (err) {
            grunt.fail.fatal(err);
        });
    });
};

var _babelPolyfill = require('babel-polyfill');

var _babelPolyfill2 = _interopRequireDefault(_babelPolyfill);

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _util = require('util');

var _util2 = _interopRequireDefault(_util);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _confluence = require('./confluence');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var fileSymbol = Symbol('files');
var titlePrefixes = {
    modules: 'Module - ',
    classes: 'Class - ',
    elements: 'Element - ',
    files: 'File - '
};

function getTitle(type, version, filename) {
    return '' + (version ? '(' + version + ') ' : '') + titlePrefixes[type] + filename;
}

function updateWikiLinks(content, version) {
    return content.replace(/\[[\s\S]*?\|([\s\S]*?)\]/g, function (match, c1) {
        if (/^(#|https?:)/.test(c1)) {
            return match;
        } else {
            return match.replace(c1, getTitle(c1.split('/')[1], version, _path2.default.parse(c1).name));
        }
    });
}
