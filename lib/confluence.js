'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.logging = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

exports.getPageFromTitle = getPageFromTitle;
exports.getMainPage = getMainPage;
exports.getAll = getAll;
exports.getChildren = getChildren;
exports.getAllPages = getAllPages;
exports.updatePage = updatePage;
exports.movePage = movePage;
exports.createPage = createPage;
exports.changeRelativeURLsToWikiLinks = changeRelativeURLsToWikiLinks;
exports.uploadFiles = uploadFiles;
exports.uploadUnder = uploadUnder;
exports.deletePage = deletePage;

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _util = require('util');

var _util2 = _interopRequireDefault(_util);

var _nodeFetch = require('node-fetch');

var _nodeFetch2 = _interopRequireDefault(_nodeFetch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var logging = exports.logging = {
    _ids: {},
    _titles: {},
    log: function log(name, message, altName) {
        if (!name || name === '0') {
            return;
        }

        var target = /\d+/.test(name) ? this._ids : this._titles;
        target[name] = target[name] || { messages: [], links: new Set() };
        target[name].messages.push({ time: Date.now(), message: message });

        if (altName) {
            var altTarget = target === this._ids ? this._titles : this._ids;
            altTarget[altName] = altTarget[altName] || { messages: [], links: new Set() };
            altTarget[altName].links.add(name);
            target[name].links.add(altName);
        }
    },
    fetch: function fetch(name) {
        var _this = this;

        var target = /\d+/.test(name) ? this._ids : this._titles;
        var messages = target[name] && target[name].messages;

        if (!messages) {
            return [];
        }

        if (target[name].links.size) {
            (function () {
                var altTarget = target === _this._ids ? _this._titles : _this._ids;
                messages = Array.from(target[name].links).reduce(function (mesgs, link) {
                    return mesgs.concat(altTarget[link].messages);
                }, messages).sort(function (messageA, messageB) {
                    return messageA.time - messageB.time;
                });
            })();
        }

        return messages.concat([{ time: Date.now(), message: 'End of current log for ' + name }, { time: Date.now(), message: '' }]);
    }
};

var processing = new Set();
var fetchQueue = new Map();

fetchQueue.enqueue = function (path, options, body) {
    var _this2 = this;

    logging.log(body.title || body.id, 'enqueuing ' + (options.method || 'GET') + ' request for ' + body.id);
    // console.log(`enqueuing ${options.method || 'GET'} request for ${body.id}`);
    return new Promise(function (resolve, reject) {
        return _this2.set(body.id, (_this2.get(body.id) || []).concat([function () {
            return resolve(path, options, body);
        }]));
    });
};

fetchQueue.dequeue = function (id) {
    var queue = this.get(id);

    if (queue) {
        logging.log(id, 'dequeing update request from ' + id);
        // console.log(`dequeing update request from ${id}`);
        if (queue.length === 1) {
            this.delete(id);
        } else {
            this.set(id, queue.slice(1));
        }

        return queue[0]();
    }
};

function qs(obj) {
    return _url2.default.format({ query: obj });
}

function f(path, options, body) {

    if (body && body.id) {
        // can cause issues (typically 500 status errors) if multiple requests to update the same page run concurrently
        if (processing.has(body.id)) {
            // console.log(`preventing multiple update requests from running on same id - ${body.id}`);
            return fetchQueue.enqueue(path, options, body).then(function (path, options, body) {
                return f(path, options, body);
            });
        } else {
            processing.add(body.id);
        }
    }

    var logId = body && body.id ? body.id : body && body.title ? body.title : body && body.space && body.space.key ? body.space.key : path.match(/(?:\?|&)title=(\w+)/) ? path.match(/(?:\?|&)title=(\w+)/)[1] : path.match(/\/content\/(\d+)/) ? path.match(/\/content\/(\d+)/)[1] : path.match(/(?:\?|&)spaceKey=(\w+)/) ? path.match(/(?:\?|&)spaceKey=(\w+)/)[1] : 0;
    if (logId) {
        logging.log(logId, 'making ' + (options.method || 'GET') + ' request to ' + options.baseUrl + path);
    } else {
        console.log(path, body);
    }
    // console.log(`${options.baseUrl}${path}`);

    var fetchOptions = {
        method: options.method || 'GET',
        body: body && (typeof body === 'undefined' ? 'undefined' : _typeof(body)) === 'object' ? JSON.stringify(body) : body ? body : null,
        headers: {
            Authorization: 'Basic ' + new Buffer(options.username + ':' + options.password).toString('base64'),
            'X-Atlassian-Token': 'nocheck',
            'Content-Type': 'application/json'
        }
    };

    return (0, _nodeFetch2.default)('' + options.baseUrl + path, fetchOptions).then(function (res) {
        if (body && body.id) {
            process.nextTick(function () {
                processing.delete(body.id);
                fetchQueue.dequeue(body.id);
            });
        }

        if (res.status >= 400 && options.method !== 'DELETE') {
            logging.log(logId, (fetchOptions.method || 'GET') + ' failed for ' + options.baseUrl + path + ' ' + (body && body.title ? body.title : '') + ' data=' + _util2.default.inspect(body));
            // console.error(`${fetchOptions.method || 'GET'} failed for ${options.baseUrl}${path} ${body && body.title ? body.title : ''}`);

            return res.text().then(function (text) {
                //console.log(text);
                logging.log(logId, text);
                logging.fetch(logId).forEach(function (log) {
                    return console.log(log.message);
                });
                return Promise.reject(text);
            });
        }

        return res.text();
    })
    // N.B. DELETE requests return an empty response body even when successfull!
    .then(function (text) {
        var json = text ? JSON.parse(text) : text;

        if (json.id || json.results && json.results[0] && json.results[0].id) {
            logging.log(logId, (options.method || 'GET') + ' request complete for ' + (json.title || json.results[0].title) + ' (' + (json.id || json.results[0].id) + ')');
        }

        return json;
    });
}

function getPageFromTitle(options) {
    logging.log((options.title || options.spaceKey).replace(/\.html$/, ''), 'getting page data from title');
    return f('/rest/api/content/' + qs({ spaceKey: options.spaceKey, title: options.title.replace(/\.html$/, ''), expand: 'version,ancestors' }), options).then(function (res) {
        return res.results[0];
    });
}

function getMainPage(options) {
    return getPageFromTitle(Object.assign({}, options, { title: '' }));
}

function getAll(endpoint, options, body) {
    var results = [];

    function go(endpoint, options, body) {
        return f(endpoint, options, body).then(function (json) {
            results = results.concat(json.results);
            if (json._links && json._links.next) {
                return go('' + json._links.next + qs(options.query).replace('?', '&'), options);
            } else {
                return results;
            }
        });
    }

    return go(endpoint, options, body);
}

function getChildren(id, options) {
    return getAll('/rest/api/content/' + id + '/child/page?expand=version,space,ancestors', options);
}

function getAllPages(options) {
    return getAll('/rest/api/content?space=' + options.spaceKey, options);
}

function updatePage(_ref, newContent, options) {
    var id = _ref.id;
    var type = _ref.type;
    var title = _ref.title;
    var version = _ref.version;

    logging.log(id, 'update page', title);
    // console.log('update page', pageData.title);

    return f('/rest/api/content/' + id + '?expand=version,ancestors', Object.assign({}, options, { method: 'PUT' }), {
        id: id,
        type: type,
        title: title,
        space: {
            key: options.spaceKey
        },
        body: {
            storage: {
                value: newContent,
                representation: 'wiki'
            }
        },
        version: {
            number: version.number + 1,
            message: 'Automated help documentation deployment'
        }
    }).then(function (pageData) {
        return pageData;
    });
}

function movePage(pageData, newAncestor, options) {
    logging.log(pageData.id, 'moving page to ancestor ' + newAncestor, pageData.title);
    // console.log(`Moving ${pageData.title} to parent with id: ${newAncestor}`);
    return f('/rest/api/content/' + pageData.id + '?expand=version,ancestors', Object.assign({}, options, { method: 'PUT' }), Object.assign({}, pageData, { ancestors: [{ id: newAncestor }], version: { number: pageData.version.number + 1 } }));
}

function findByTitleExtensive(fuzzyTitle, options) {
    function go(endpoint, options) {
        logging.log(fuzzyTitle, 'non matching cases on page name - performing extensive search for ' + fuzzyTitle);
        return f(endpoint, options).then(function (json) {
            var match = json.results.find(function (_ref2) {
                var title = _ref2.title;

                console.log(title);
                return title.toLowerCase() === fuzzyTitle.toLowerCase();
            });
            if (match) {
                return match;
            } else if (json._links.next) {
                return go('' + json._links.next, options);
            } else {
                return null;
            }
        });
    }

    return go('/rest/api/content/search/?spaceKey=' + options.spaceKey + '&cql=space=' + options.spaceKey + '%20AND%20title%~%20"' + fuzzyTitle.split(' ')[0] + '"&expand=version%2Cancestors', options);
}

function createPage(parentId, title, spaceKey, content, options) {
    logging.log(title, 'createPage');
    var body = {
        type: 'page',
        title: title,
        ancestors: !parentId ? null : [{
            'id': parentId
        }],
        'space': {
            'key': spaceKey
        },
        body: {
            storage: {
                value: content,
                representation: 'wiki'
            }
        }
    };

    return f('/rest/api/content/?expand=version,ancestors', Object.assign({}, options, { method: 'POST' }), body).catch(function (e) {
        // response body in error might be html or JSON
        try {
            e = JSON.parse(e);
        } catch (err) {}

        // if page already exists in the space but is not a child of the correct page get the information in order to update and move it to where we need it to be
        if (e && /A page with this title already exists/.test(e.message)) {
            logging.log(title, 'page already exists with title "' + title + '"');
            return getPageFromTitle(Object.assign({}, options, { title: title })).then(function (pageData) {
                if (!pageData) {
                    /*
                        Confluence pages are saved in a case-insensitive format but the REST API is case sensitive
                        this means page creation can be rejected but when searching for the page using the title it may find nothing
                        because the title case does not match. In this case we have to perform an extensive and slow search of similar title
                        pages so that we can find the correct page to move *sigh*
                    */
                    return findByTitleExtensive(title, options).then(function (pageData) {
                        logging.log(pageData.id, 'found page by extensive search with id', title);
                        return movePage(Object.assign({}, pageData, { title: title }), parentId, options);
                    });
                }
                return movePage(pageData, parentId, options);
            }).then(function (pageData) {
                return updatePage(pageData, content, options);
            });
        }

        return Promise.reject(e);
    });
}

function changeRelativeURLsToWikiLinks(content) {
    return content.replace(/\[[\s\S]*?\|([\s\S]*?)\]/g, function (fullMatch, c1) {
        return fullMatch.replace(c1, urlToWikiUrl(c1));
    });
}

function uploadFiles(parentId, fileArray, pages, options) {
    // console.log('uploadFiles', parentId, pages.length);
    return Promise.all(fileArray.map(function (_ref3) {
        var path = _ref3.path;
        var title = _ref3.title;
        var content = _ref3.content;

        var matchingPage = pages.find(function (page) {
            return page.title === title;
        });
        if (matchingPage) {
            logging.log(matchingPage.id, 'found existing page with this title as a child of ' + parentId, matchingPage.title);
            return updatePage(matchingPage, content, Object.assign({}, options, { title: title }));
        } else {
            logging.log(title, 'no existing page found with title "' + title + '" as a child of ' + parentId);
            return createPage(parentId, title, options.spaceKey, content, options);
        }
    }));
}

function uploadUnder(_ref4, options, fileMap) {
    var parentId = _ref4.id;
    var parentTitle = _ref4.title;

    var writtenIds = new Set([parentId]);
    var existingIds = new Set([parentId]);
    var subDirs = Object.keys(fileMap);

    logging.log(parentId, 'uploading content to ' + parentTitle, parentTitle);
    // console.log('uploadUnder', parentId);

    return getChildren(parentId, options).then(function (children) {
        return children.map(function (child) {
            return existingIds.add(child.id) && child;
        });
    }).then(function (children) {
        return uploadFiles(parentId, fileMap[options.fileSymbol], children, options);
    }).then(function (writtenPages) {
        return writtenPages.forEach(function (page) {
            return writtenIds.add(page.id);
        });
    }).then(function () {
        return Promise.all(subDirs.map(function (dir) {
            return getPageFromTitle(Object.assign({}, options, { title: dir }));
        }));
    }).then(function (pageArray) {
        return Promise.all(pageArray.map(function (page, i) {
            // try to find the directory pages, create if they don't exist or move them if they are not a descendant of the correct parent page
            if (!page) {
                logging.log(subDirs[i], 'directory not found in space - must be created');
                return createPage(parentId, subDirs[i], options.spaceKey, options.directoryContent, options).then(function (content) {
                    return writtenIds.add(content.id) && content;
                });
            } else if (page.ancestors.some(function (ancestor) {
                return ancestor.id === parentId;
            }) === false) {
                logging.log(subDirs[i], 'directory found in space but under wrong parent - must be moved');
                writtenIds.add(page.id);
                return movePage(page, parentId, options);
            }

            logging.log(subDirs[i], 'directory found in space under correct parent');
            return Promise.resolve(page);
        }));
    }).then(function (pages) {
        return Promise.all(pages.map(function (page) {
            return uploadUnder(page, options, fileMap[page.title]);
        }));
    }).then(function (idSets) {
        return (idSets || []).reduce(function (results, idSet) {
            return {
                written: Array.from(idSet.written).reduce(function (written, writtenPage) {
                    return written.add(writtenPage);
                }, results.written),
                existing: Array.from(idSet.existing).reduce(function (existing, existingPage) {
                    return existing.add(existingPage);
                }, results.existing)
            };
        }, { written: writtenIds, existing: existingIds });
    });
}

function deletePage(id, options) {
    console.log('deleting superfluous page with id ' + id);
    return f('/rest/api/content/' + id, Object.assign({}, options, { method: 'DELETE' })).then(function (text) {
        return text !== '' ? 'Failed to delete ' + id : true;
    }).catch(function () {
        return 'Failed to delete ' + id;
    });
}
