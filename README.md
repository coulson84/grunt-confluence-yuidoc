Installation
============

`npm install grunt-confluence-docs --save-dev`

Usage
=====

```javascript
grunt.initConfig({
    confluenceUpload: {
        main: {
            src: 'docs/**',
            options: {
                confluence: {
                    username: '',
                    password: '',
                    spaceKey: '',
                    title: '',
                    version: grunt.file.readJSON('package.json').version.split('.').slice(0,2).join('.'),
                    baseUrl: 'https://atlassian.domain/wiki',
                    directoryContent: '{children:depth=2|sort=title}'
                }
            }
        }
    }
});
```


Options
=======

* username - (required) The atlassian username that will be used for all REST api requests. This will default to process.env.CONFLUENCE_USERNAME if not provided. Generally it shouldn't be included in any source control history.
* password - (required) The atlassian username's password used for authenticating REST api requests. This will default to process.env.CONFLUENCE_PASSWORD if not provided. Generally it shouldn't be included in any source control history.
* baseUrl - (required) The base url that will be used for making all REST API requests defaults to process.env.CONFLUENCE_URL.
* spaceKey - (required) The code for the space in the wiki that the content will be uploaded to. Will default to process.env.CONFLUENCE_SPACE.
* title - (optional) The title of the parent page that the documentation will be uploaded underneath. If not provided it will be uploaded under the welcome page of the space. Defaults to process.env.CONFLUENCE_TITLE.
* version - (optional) The version that the documentation will uploaded under. If you don't want to separate out documentation by versions then this field can be left blank or in any falsy state. The version will be used exacly as passed - if there is also a title passed the version page will be created underneath  the title page. If there is no title passed in the options then the version will be uploaded directly under the home page of the spaceKey.
* directoryContent - (optional) For each directory contained in the documentation an empty page will be inserted. You can enter confluence wiki markup here that will be inserted instead. The above example will show the child pages within the directory.
