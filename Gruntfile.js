module.exports = function(grunt) {
    'use strict';
    let pkg = grunt.file.readJSON('package.json');

    grunt.initConfig({
        babel: {
            options: {
                sourceMap: false,
                presets: ['babel-preset-stage-0', 'babel-preset-es2015']
            },
            dist: {
                files: [{
                    cwd: 'src/',
                    src: '**/*.js',
                    dest: 'lib/',
                    expand: true
                }]
            }
        },
        watch: {
            dev: {
                files: ['src/**'],
                tasks: ['babel']
            }
        }
    });

    Object.keys(pkg.dependencies || {}).concat(Object.keys(pkg.devDependencies || {})).forEach(function(key) {
        if (key !== 'grunt' && key.indexOf('grunt') === 0) {
            grunt.loadNpmTasks(key);
        }
    });
};
